package sample;

import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;


public class AnimatePointerController implements Initializable{

    @FXML
    private GridPane gridParent;

    private List<Pointer> pointers = new ArrayList<>();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        int countMills = 1000;
        int c = 0;
        for (int i = 1; i <= 8; i++) {
            SequentialTransition seq = animateAllPointers(10);
            seq.setDelay(Duration.millis(countMills));
            for (int k = c; k < 6*i; k++) {
                setKeyEvent(pointers.get(k));
            }
            seq.play();
            countMills+=400;
            c+=3;
        }
       SequentialTransition sequentialTransition = new SequentialTransition(animateAllPointers(6));
       sequentialTransition.play();
        SequentialTransition sequentialTransition1 = new SequentialTransition(animateAllPointers(6));
        sequentialTransition1.play();
        sequentialTransition.play();
    }
    private SequentialTransition animateAllPointers(int count){
        SequentialTransition seq = new SequentialTransition();
        for (int i = 0; i < count; i++) {
            Pointer pointer = createNewPointer();
            pointers.add(pointer);
            setKeyEvent(pointer);
            TranslateTransition transition = new TranslateTransition(Duration.seconds(5), pointer.getImageView());
            transition.setToX(-800);
            transition.setDelay(Duration.millis(600));
            transition.setOnFinished(event ->
                gridParent.getChildren().remove(pointer.getImageView())
            );
            seq.getChildren().add(transition);
        }
        return seq;
    }
    private void setKeyEvent(Pointer pointer) {

        EventHandler<KeyEvent> keyPressed = event -> {
            System.out.println("<<<<<<<  " + pointer.getType());
            KeyCode keyCode = null;
            switch (pointer.getType()) {
                case 0:
                    keyCode = KeyCode.LEFT;
                    break;
                case 1:
                    keyCode = KeyCode.RIGHT;
                    break;
                case 2:
                    keyCode = KeyCode.DOWN;
                    break;
                case 3:
                    keyCode = KeyCode.UP;
                    break;
            }
            if (event.getCode() == keyCode) {
                System.out.println(keyCode.getName());
                pointer.getImageView().setEffect(new ColorAdjust(0,0,0.46, 0));
            } else {
                System.out.println("wrong key");
            }
        };

        EventHandler<KeyEvent> keyReleased = event -> {
            pointer.getImageView().removeEventHandler(KeyEvent.KEY_PRESSED, keyPressed);
        };
        pointer.getImageView().setOnKeyPressed(keyPressed);
        pointer.getImageView().setOnKeyReleased(keyReleased);
    }




    private Pointer createNewPointer(){
        Pointer newPointer = new Pointer(randomPointer());
        newPointer.getImageView().setFitWidth(100);
        newPointer.getImageView().setFitHeight(100);
        newPointer.getImageView().setTranslateX(800);
        newPointer.getImageView().setTranslateY(450);
        newPointer.getImageView().setFocusTraversable(true);
        gridParent.getChildren().add(newPointer.getImageView());
        return newPointer;
    }

    private int randomPointer(){
        Random random = new Random();
        return random.nextInt(4);
    }
}
