package sample;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class Pointer {
    private Image image;
    private ImageView imageView;
    private int type;

    public Pointer(int type) {
        image = new Image("img/" + type + ".png");
        imageView = new ImageView(image);
        this.type = type;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
